<?php
/**
 * Created by PhpStorm.
 * User: Sara
 * Date: 9/21/2018
 * Time: 4:29 PM
 */

class User
{
    private $db;
    public function __construct()
    {
        $this->db =  new Database;
    }
    public function login($email,$password){
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email',$email);
        $record  = $this->db->getOne();
        $hashed_pass = $record->password;
        if(password_verify($password,$hashed_pass)){
            return $record;
        }
        else {
            return false;
        }
    }
    public function register($data){
        $this->db->query('INSERT INTO users (name,email,password) VALUES (:name,:email,:password)');
        $this->db->bind(':name',$data['name']);
        $this->db->bind(':email',$data['email']);
        $this->db->bind(':password',$data['password']);
        if ($this->db->execute()){
            return true;
        }
        else{
            return false;
        }
    }
    public function findByEmail($email){
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email',$email);
        $record = $this->db->getOne();
        if ($this->db->rowCount()>0){
            return true;
        }
        else{
            return false;
        }
    }

}