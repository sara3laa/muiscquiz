<?php
/**
 * Created by PhpStorm.
 * User: Sara
 * Date: 9/19/2018
 * Time: 2:43 AM
 */
class Database{
    private $host = DBHOST;
    private $user = DBUSER;
    private $pass = DBPASS;
    private $dbname = DBNAME;

    private $dbh;
    private $stmt;
    private $error;
    public function __construct()
    {
        $dsn = 'mysql:host='. $this->host . ';dbname='. $this->dbname;
        $options =[
            PDO::ATTR_PERSISTENT =>true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];
        try{
            $this->dbh = new PDO($dsn,$this->user,$this->pass,$options);
        }catch (PDOException $e){
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }

    public function query($sql){
        $this->stmt = $this->dbh->prepare($sql);
    }
    public function bind($params, $value, $type=null){
        if (is_null($type)) {
            if (is_bool($value)) {
                $type = PDO::PARAM_BOOL;
            } elseif (is_int($value)) {
                $type = PDO::PARAM_INT;
            } elseif (is_null($value)) {
                $type = PDO::PARAM_NULL;
            } else {
                $type = PDO::PARAM_STR;
            }
        }
       $this->stmt->bindValue($params,$value,$type);
    }
   public function execute(){
        return $this->stmt->execute();
    }
    public function getAll(){
        $this->execute();

        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }
    public function getOne(){
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }
    public function rowCount(){
        return $this->stmt->rowCount();
    }
}