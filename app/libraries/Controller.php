<?php
/**
 * Created by PhpStorm.
 * User: Sara
 * Date: 9/19/2018
 * Time: 2:44 AM
 */
class Controller{
    public function model($model){
      require_once '../app/models/'. $model.'.php';
      return new $model();
    }
    public function view($view,$data=[]){
        if (file_exists('../app/views/'.$view.'.php')){
            require_once '../app/views/'.$view.'.php';
        }
        else{
            die('Action dose not exist');
        }
    }
}