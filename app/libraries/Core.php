<?php
/**
 * Created by PhpStorm.
 * User: Sara
 * Date: 9/19/2018
 * Time: 2:41 AM
 */

#URL Format /Controller/method/params

class Core
{
    protected $currController = 'Welcome';
    protected $currAction = 'index';
    protected $params = [];
    public function __construct()
    {
       $url= $this->getUrl();
       if (file_exists('../app/controllers/' . ucwords($url[0]). '.php')){
           $this->currController = $url[0];
           unset($url[0]);
       }
       require_once '../app/controllers/'. $this->currController.'.php';
       $this->currController = new  $this->currController;
       if (isset($url[1])){
           if (method_exists($this->currController,$url[1])){
               $this->currAction=$url[1];
               unset($url[1]);
           }
       }
       $this->params = $url ? array_values($url):[];
       call_user_func_array([$this->currController,$this->currAction],$this->params);

    }

    public function getUrl()
    {
        if (isset($_GET['url'])) {
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
            return $url;
        }
    }

}