<?php
/**
 * Created by PhpStorm.
 * User: Sara
 * Date: 9/19/2018
 * Time: 7:29 PM
 */
define('DBHOST','localhost');
define('DBUSER','root');
define('DBPASS','');
define('DBNAME','musicquizgame');
define('APPROOT',dirname(dirname(__FILE__)));

define('URLROOT','http://localhost/musicquiz');
define('SITENAME','MusicQuiz');