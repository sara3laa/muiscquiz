<?php
/**
 * Created by PhpStorm.
 * User: Sara
 * Date: 9/19/2018
 * Time: 2:52 AM
 */
require_once 'config/config.php';
require_once 'helpers/_helper.php';
spl_autoload_register(function ($className) {
    require_once 'libraries/'.$className.'.php';
});