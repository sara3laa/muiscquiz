<?php
/**
 * Created by PhpStorm.
 * User: Sara
 * Date: 9/20/2018
 * Time: 9:27 PM
 */
?>

<nav class="navbar navbar-expand-lg navbar-dark blue-gradient ">
    <div class="container">
    <a class="navbar-brand" href=<?php echo URLROOT?> > <?php  echo SITENAME?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor03">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href=<?php echo URLROOT?>>Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT?>/quizzes/index">Quiz</a>
            </li>
        </ul>
        <ul class="navbar-nav ">

            <?php if(isset($_SESSION['user_id'])) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo URLROOT?>/users/logout">Logout</a>
                </li>
            <?php else : ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT?>/users/register">Register</a>

            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT?>/users/login">Login</a>
            </li>
            <?php endif; ?>
        </ul>
    </div>
    </div>
</nav>