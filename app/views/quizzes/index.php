
<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="row">
    <div class="col-md-6">
        <h1 class="txtcolor">Quizzes</h1>
    </div>
    <div class="col-md-6 my-2">
        <a href="<?php echo URLROOT; ?>/quizzes/take" class="btn btn-success pull-right">
            <i class="fa fa-star"> Take Quiz</i>
        </a>
    </div>
</div>
<?php
   foreach ($data['quizzes'] as $quiz):?>
        <div class="card card-body mb-3">
            <h4 class="card-title"><?php echo $quiz->question;?></h4>
        </div>
    <?php endforeach; ?>
<?php require APPROOT. '/views/inc/footer.php'; ?>