<?php require APPROOT . '/views/inc/header.php'; ?>

<form action="<?php echo URLROOT ?>/quizzes/take" method="Post">
    <script>

        var game_question =  <?php echo json_encode($data['question_data']);  ?>;
        var gam_answer = <?php echo json_encode($data['answers']);  ?>
    </script>

    <div class="deliceta-gradient content p-5 m-4">
        <h1 class="gamehead text-center my-3">Quiz Time !</h1>

        <div id="score" class="p-1 m-1 pull-left btn">
            Score: <span id="scoreValue">0</span>
        </div>
        <div id="timeremaining" class="p-1 m-1 pull-right">
            Timer: <span id="timeremainingvalue">60</span> sec
        </div>

        <div class="container-fluid p-4  d-flex justify-content-center align-items-center h-100">
            <div id="gameOver"></div>
            <div class="card" style="width: 35rem;">
                <div class="card-header">
                    <h4><i class="fa fa-question-circle"></i><span id="question">bla?</span></h4>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><i class="	fa fa-heart"></i>
                        <button class="btn greenblue-gradient"><span id="btn-1">x</span></button>
                    </li>

                    <li class="list-group-item"><i class="	fa fa-heart"></i>
                        <button class="btn greenblue-gradient"><span id="btn-2">y</span></button>
                    </li>
                    <li class="list-group-item"><i class="	fa fa-heart"></i>
                        <button class="btn btncolor greenblue-gradient"
                        <span id="btn-3">z</span></button></li>
                    <li class="list-group-item"><i class="	fa fa-heart"></i>
                        <button class="btn btncolor greenblue-gradient"><span id="btn-4">zz</span></button>
                    </li>
                </ul>

            </div>
        </div>

        <button class="monto-gradient btn my-1 center"><span id="startreset">Start Game</span></button>


    </div>



    <input type="submit" value="Next"  id="next" class="btn btn-success btn-block ">
</form>


    <?php require APPROOT . '/views/inc/footer.php'; ?>

