<?php
/**
 * Created by PhpStorm.
 * User: Sara
 * Date: 9/19/2018
 * Time: 7:06 PM
 */
?>
<?php  require APPROOT . '/views/inc/header.php';?>
<div class=" jumbotron jumbotron-fluid">
    <div class="container"></div>
    <h1 class="display-2"><?php echo $data['title']; ?></h1>
    <p class="lead"><?php echo  $data['description'];?> </p>
</div>

<?php  require APPROOT .'/views/inc/footer.php'; ?>