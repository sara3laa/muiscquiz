<?php
/**
 * Created by PhpStorm.
 * User: Sara
 * Date: 9/19/2018
 * Time: 6:57 AM
 */

class Welcome extends Controller
{

    public function __construct()
    {

    }

    public function index(){
        $data =[
            'title' => 'Music Quiz',
            'description' => 'Test yourself on music questions . What is your level of music expertise?'
        ];
        $this->view('welcome/index',$data);

    }

    public function about(){
        $this->view('welcome/about');
    }




}