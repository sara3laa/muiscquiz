<?php

/**
 * @property mixed userModel
 */
class Users extends Controller
{
    public function __construct()
    {
        $this->userModel =$this->model('User');

    }

    public function login()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => ''
            ];
            $data['email_err'] = $this->emptyValidate($data['email'],'Your E-mail').
            $this->emailValidatelog($data['email']);
            $data['password_err'] = $this->emptyValidate($data['password'],' Your Password');
            if (empty($data['password_err'].$data['email_err'])){
                $loggedInUser = $this->userModel->login($data['email'],$data['password']);
                if ($loggedInUser){
                    $this->createUserSession($loggedInUser);
                }else{
                    $data['password_err']='Password incorrect';
                    $this->view('users/login', $data);
                }
            }
            else{
                $this->view('users/login', $data);
            }
        } else {
            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => ''
            ];
        }
        $this->view('users/login', $data);
    }

    public function index()
    {

    }

    public function register()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'name' => trim($_POST['name']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'confirm_password' => trim($_POST['confirm_password']),
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_pass_err' => ''
            ];
            $data['name_err'] = $this->emptyValidate($data['name'], 'Name');
            $data['email_err'] = $this->emptyValidate($data['email'], 'E-mail').
            $this->emailValidateReg($data['email']);
            $data['password_err'] = $this->emptyValidate($data['password'], 'Password') .
                $this->rangeValidate($data['password'], 'password');
            $data['confirm_pass_err'] = $this->emptyValidate($data['confirm_password'],'Confirm Password')
                .$this->confirmValidate($data['password'],$data['confirm_password'],'Password');
            if (empty($data['name_err'].$data['email_err']. $data['password_err']. $data['confirm_pass_err'])){
                $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
                if ($this->userModel->register($data)){
                    flash('register_success','You Joined the club Geeky :D');
                    redirect('users/login');
                }
                else{
                    die("FAILED REGISTER");
                }
            }
            else{
                $this->view('users/register', $data);
            }

        } else {
            $data = [
                'name' => '',
                'email' => '',
                'password' =>null,
                'confirm_password'=>null,
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_pass_err' => ''
            ];
        }
        $this->view('users/register', $data);
    }

    private function emptyValidate($data_field, $str_var)
    {
        if (empty($data_field)) {
            return "Please Enter " . $str_var;
        }
        return '';
    }

    private function rangeValidate($data_field, $str_var)
    {
        if (strlen($data_field) < 6) {
            return "Your " . $str_var . " must be at least 6 characters";
        }
        return '';
    }

    private function confirmValidate($data_field1, $data_field2, $str_var)
    {
        if ($data_field1 != $data_field2) {
            return  $str_var." mismatch";
        }
        return '';
    }
   private function emailValidateReg($email){
        $usermail = $this->userModel->findByEmail($email);
        if ($usermail){

                  return 'Email is already taken';
              }

        return '';
   }
   private  function emailValidatelog($email){
       if($this->userModel->findByEmail($email)){
           return '';
       }
       return 'E-mail not registered';
   }
 public function createUserSession($loggedUser){
        $_SESSION['user_id'] = $loggedUser->id;
        $_SESSION['user_email'] = $loggedUser->email;
        $_SESSION['user_name'] = $loggedUser->name;
        redirect('quizzes/index');
 }
 public function logout(){
        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_name']);
        session_destroy();
        redirect('users/login');
 }
    public  function isLoggedIn(){
        if (isset($_SESSION['user_id'])){
            return true;
        }
        else{
            return false;
        }
    }
}