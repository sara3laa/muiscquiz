<?php
/**
 * Created by PhpStorm.
 * User: Sara
 * Date: 9/23/2018
 * Time: 12:47 AM
 *
 * @property mixed quizModel
 */

class Quizzes extends Controller
{
    public function __construct()
    {
      $this->quizModel = $this->model('Quiz');
    }

    public function  index(){
        $demoQuizzes = $this->quizModel->getDemoQuizzes();
        $data=['quizzes'=>$demoQuizzes];
        $this->view('quizzes/index',$data);
    }
    public function take(){
        if(isUserLoggedIn()) {
                 $this->answerQuiz();
                 if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                     $this->answerQuiz();
                 }
        }else{
            redirect('users/login');
        }

    }


    private function answerQuiz(){
        $quizzes = $this->quizModel->getQuizzes();
        $answgnere = $this->quizModel->getAnswers($quizzes->answerId);
        $data=['question_data'=>$quizzes,
            'answers'=>$answgnere] ;



        $this->view('quizzes/take',$data);
    }
}